var gulp       = require('gulp');
var requireDir = require('require-dir');
var dir        = requireDir('./tasks');
var path       = require('path');
var connect    = require('gulp-connect');

//option
var paths = {
  srcDir : 'src',
  dstDir : 'dst'
}

//serve
gulp.task('serve', function(){
  connect.server({
    root: paths.dstDir,
    livereload: true
  });
});

//watch
gulp.task('watch', ['sass', 'copy'], function(){
  var scssWatchGlob = paths.srcDir + '/scss/*.scss';
  var htmlWatchGlob = paths.srcDir + '/*.html';

  gulp.watch( scssWatchGlob, ['sass']);
  gulp.watch( htmlWatchGlob, ['copy']);
});


//default task
gulp.task('default', ['watch', 'serve']);