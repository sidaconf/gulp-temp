# gulp template

個人用のgulpを使用したテンプレートです。


## Getting Started

### Required Components

- [Node.js](http://sanographix.github.io/rin-html-template/)
- LiveReload [for Chrome](http://sanographix.github.io/rin-html-template/) / [for Firefox](http://sanographix.github.io/rin-html-template/)


## set up

### gulpのインストール

ターミナルで `gulp -v` を実行し、gulpが入っていない場合は下記を実行し、
gulpをグローバルにインストールする。

```
$ sudo npm install --global gulp
```

プロジェクトフォルダへ移動し、下記を実行。

```
$ git clone git@bitbucket.org:sidaconf/gulp-temp.git test
$ cd test
$ npm install
```

`package.json` の内容で構成されます。



## フォルダ構成

```
[project]
├── [dist] - 書き出し用フォルダ
│   ├── [css]
│   ├── [img]
│   │   ├── [2x] - src/img/org/から200pxで縮小・圧縮書き出し
│   │   └── [org] - 圧縮・書き出し
│   └── index.html
│
├── [src] - メインで編集するフォルダ
│   ├── [scss]
│   ├── [img]
│   │   └── [org] - レスポンシブ用画像の圧縮・書き出し元
│   └── index.html
│
├── [tasks]
│   ├── copy.js - htmlファイルコピー用のタスク
│   ├── imagemin.js - 画像圧縮用のタスク
│   ├── responsive.js - レスポンシブ対応画像用のタスク
│   └── sass.js - sassコンパイル用のタスク
│
├── gulpfile.js - gulp実行用のメインファイル
├── package.json
└── README.md
```


## gulpの起動

```
$ gulp
```

上記実行でファイルの監視・ローカルサーバーの立ちあげを実行する。
終了は <kbd>Ctrl + c</kbd>

### 画像の圧縮

```
$ gulp imagemin
```

上記実行で `src/img/` フォルダ内の画像を圧縮して `dst/img/` へ書き出すことができる。


### レスポンシブ対応画像の書き出し、圧縮

```
$ gulp image-optim
```

上記実行でレスポンシブ対応画像の書き出し、圧縮ができる。
デフォルト設定で `200px` で書きだすので、変更の際は `tasks/responsive.js` タスクの、

```
var responsiveOptions = [{
  name: '*.png',
  width: 200
}];
```

の行にある `200` の数値を変更する。


### ローカルサーバー

デフォルトで `http://localhost:8080` と設定されています。

まだまだ調整中です。
