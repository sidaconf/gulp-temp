var gulp       = require('gulp');
var path       = require('path');
var sass       = require('gulp-ruby-sass');
var plumber    = require('gulp-plumber');
var notify     = require('gulp-notify');
var connect    = require('gulp-connect');
var cache      = require('gulp-cached');

//option
var paths = {
  srcDir : 'src',
  dstDir : 'dst'
}

//task
gulp.task('sass', function(){
  var srcGlob = paths.srcDir + '/scss/*.scss';
  var dstGlob = paths.dstDir + '/css';
  var errorMessage = 'Error: <%= error.message %>';
  var sassOptions = {
    style : 'nested',
    noCache : true,
    "sourcemap=none": true
  }

  gulp.src( srcGlob )
    .pipe(cache( 'sass' ))
    .pipe(plumber({
      errorHandler: notify.onError( errorMessage )
    }))
    .pipe(sass( sassOptions ))
    .pipe(gulp.dest( dstGlob ))
    .pipe(connect.reload());
});
