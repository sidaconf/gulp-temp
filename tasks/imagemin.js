var gulp     = require('gulp');
var imagemin = require('gulp-imagemin');
var changed = require('gulp-changed');

var paths = {
	srcDir : 'src/img',
	dstDir : 'dst/img'
}

gulp.task( 'imagemin', ['responsive'], function(){
	var srcGlob = paths.srcDir + '/**/*.+(jpg|jpeg|png|gif|svg)';
	var dstGlob = paths.dstDir;
	var imageminOptions = {
		optimizationLevel: 7
	};

	gulp.src( srcGlob )
		.pipe(changed( dstGlob ))
		.pipe(imagemin( imageminOptions ))
		.pipe(gulp.dest( dstGlob ));
});
