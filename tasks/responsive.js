var gulp       = require('gulp');
var responsive = require('gulp-responsive');

var paths = {
  srcDir : 'src/img',
  dstDir : 'dst/img'
}

gulp.task('responsive', function () {
  var srcGlob = paths.srcDir + '/org/*.png';
  var dstGlob = paths.dstDir + '/2x';
  var responsiveOptions = [{
    name: '*.png',
    width: 200
  }];

  return gulp.src( srcGlob )
    .pipe(responsive( responsiveOptions ))
    .pipe(gulp.dest( dstGlob ));
});

gulp.task('image-optim', ['imagemin']);
