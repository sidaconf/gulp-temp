var gulp       = require('gulp');
var connect    = require('gulp-connect');

//option
var paths = {
  srcDir : 'src',
  dstDir : 'dst'
}

//task
gulp.task('copy', function(){
  var srcGlob = paths.srcDir + '/*.html';
  var dstGlob = paths.dstDir;

  gulp.src( srcGlob )
    .pipe(gulp.dest( dstGlob ))
    .pipe(connect.reload());
});